# Base adresse locale


Spécification du modèle de données relatif aux adresses locales d’une collectivité (BAL)

- nom : scdl-adresses
- page d'accueil : https://git.opendatafrance.net/scdl/adresses
- URL du schéma : https://git.opendatafrance.net/scdl/adresses/raw/v1.1.4/schema.json
- version : 1.1.4
- date de création : 30/05/2018
- date de dernière modification : 27/06/2019
- concerne le pays : FR
- valeurs manquantes représentées par : `[""]`
- contributeurs :
  - OpenDataFrance (auteur)
- ressources :
  - Exemple de fichier adresses invalide ([lien](https://git.opendatafrance.net/scdl/adresses/raw/v1.1.4/exemples/exemple_invalide.csv))

## Modèle de données

Ce modèle de données repose sur les 13 champs suivants correspondant aux colonnes du fichier tabulaire.

### `cle_interop`

- titre : Clé d'interopérabilité
- description : Cette clé combine le [code INSEE de la commune](https://fr.wikipedia.org/wiki/Code_Insee) sur 5 caractères (incluant 'A' ou 'B' pour la Corse) + le code de voie issu du [FANTOIR](https://fr.wikipedia.org/wiki/FANTOIR) de la DGFiP sur 4 caractères + le numéro d’adresse sur 5 caractères préfixé par des zéros + un suffixe s'il existe, qui peut être un indice de répétition ('bis', 'ter', 'qua', 'qui', etc... codés sur 3 caractères) et/ou un complément ('a', 'b', 'c', 'a1', 'b2', 'lesmimosas', etc... sans limitation du nombre de caractères). Chaque élément est séparé par un tiret du bas et les lettres sont en minuscule.
- type : chaîne de caractères
- exemple : `35238_3961_00007_bis`
- valeur obligatoire
- taille minimale : 16
- motif : `^[A-Za-z0-9_]+$`

### `uid_adresse`

- titre : Identifiant unique national d’adresse
- description : Cet identifiant unique d’adresse est géré et attribué par le service "guichet national d’identification" de la Base Adresse Nationale. Dans l'attente de la mise en place de ce service, les règles de création ou de gestion de cet identifiant ne sont pas connues. La valeur de ce champ est donc optionnelle et sera laissée vide aussi longtemps que le service d'identification ne sera pas opérationnel.
- type : chaîne de caractères (uuid)
- valeur optionnelle

### `voie_nom`

- titre : Nom complet de la voie
- description : Ce champ contient la concaténation du type et du nom de la voie ou le nom d'un lieu-dit, exprimés en majuscules et minuscules accentuées.
- type : chaîne de caractères
- exemple : `'Allée de Bréhat' pour une concaténation ou 'Le pré aux grenouilles' pour un lieu-dit`
- valeur obligatoire
- taille minimale : 3
- motif : `^[a-zA-Z0-9\-\'\s\d\u00C0-\u00FF]+$`

### `numero`

- titre : Numéro d’adresse
- description : Numéro d’adresse dans la voie et, dans le cas des voies sans adresse, la valeur “99999” est attendue
- type : nombre entier
- exemple : `130`
- valeur obligatoire
- valeur maximale : 99 999

### `suffixe`

- titre : Information suffixée qui complète et précise le numéro d’adresse
- description : Cette information peut être un indice de répétition ('bis', 'ter', 'qua', 'qui', etc... codés sur 3 caractères en minuscules) ou un complément comme le nom d'entrée d'immeuble ('a', 'b', 'c', 'a1', 'b2', 'lesmimosas', etc... codés en minuscules non accentuées, sans espace ni limite du nombre de caractères).
- type : chaîne de caractères
- exemple : `'ter' pour un indice de répétition ou 'lesmimosas' pour un nom d'entrée d'immeuble`
- valeur optionnelle
- motif : `^[a-z\d\u00DF-\u00FF]+$`

### `commune_nom`

- titre : Nom officiel de la commune
- description : Ce champ doit permettre d’identifier rapidement le territoire concerné et d'éviter les quiproquos.
- type : chaîne de caractères
- exemple : `Brest`
- valeur obligatoire
- motif : `^[A-Za-z\s\-\u00C0-\u00FF]+$`

### `position`

- titre : Code de position de l’adresse
- description : Ce code décrit la position d’une adresse à partir d’une liste de valeurs qui provient de la spécification INSPIRE v 3.1 sur le thème "Adresses". Les valeurs attendues sont, au choix, 'délivrance postale', 'entrée', 'bâtiment', 'cage d’escalier', 'logement', 'parcelle', 'segment', ou 'service technique'.
- type : chaîne de caractères
- exemple : `cage d’escalier`
- valeur obligatoire
- valeurs autorisées : `["délivrance postale","entrée","bâtiment","cage d’escalier","logement","parcelle","segment","service technique"]`

### `x`

- titre : Coordonnée X
- description : Coordonnée X du système légal en vigueur sur le territoire concerné, conformément à l’article 1 du [décret n° 2006-272](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000000813996) du 3 mars 2006. Le signe de séparation entre les parties entière et décimale du nombre est le point.
- type : nombre réel
- exemple : `145377.5`
- valeur optionnelle

### `y`

- titre : Coordonnée Y
- description : Coordonnée Y du système légal en vigueur sur le territoire concerné, conformément à l’article 1 du [décret n° 2006-272](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000000813996) du 3 mars 2006. Le signe de séparation entre les parties entière et décimale du nombre est le point.
- type : nombre réel
- exemple : `6835665.67`
- valeur optionnelle

### `long`

- titre : Longitude
- description : Coordonnée de longitude exprimée en [WGS 84](https://fr.wikipedia.org/wiki/WGS_84). Le signe de séparation entre les parties entière et décimale du nombre est le point.
- type : nombre réel
- exemple : `-4.502217943385534`
- valeur optionnelle

### `lat`

- titre : Latitude
- description : Coordonnée de latitude exprimée en [WGS 84](https://fr.wikipedia.org/wiki/WGS_84). Le signe de séparation entre les parties entière et décimale du nombre est le point.
- type : nombre réel
- exemple : `48.383985827041485`
- valeur optionnelle

### `source`

- titre : Nom de la source
- description : Ce nom peut désigner, au choix, la collectivité ayant créé juridiquement l’adresse (par délibération), l'entité ayant créé la donnée, ou l’entité ayant diffusé / publié la donnée.
- type : chaîne de caractères
- exemple : `Rennes Métropole`
- valeur obligatoire
- motif : `[a-zA-Z0-9\-\d\s\u00C0-\u00FF]+`

### `date_der_maj`

- titre : Date de dernière mise à jour
- description : Cette date est celle de la dernière mise à jour connue de la donnée. Elle ne correspond pas à la date de publication du jeu de données en open data. Elle est exprimée au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2014-10-01`
- valeur obligatoire


